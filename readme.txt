======================================================
Sphinx-Docreplace
======================================================

---------------
Overview
---------------
# (c) 2012 James King (except as noted in the code)

This is a replacement extension for `Sphinx <http://sphinx.pocoo.org/>`_
which designed to allow for the replacing of __doc__ text
using either simple string replace methods or more
complex regex methods.

Currently, the only way to make it work is to replace the actual
sphinx.ext.autodoc module with this one. If someone else
knows how to better do this, input is appreciated.

----

The example use case was to replace inline latex math comments
or other combinations of text with the appropriate :math:`<stuff>`
nodes so that it could be processed by pngmath or similar in
Sphinx.

----
Also, you need to add the following lines to your conf.py file:
(e.g.)

# Stuff for docreplace:
docreplace_run_replace = True;
#docreplace_simple_replace = {};
#docreplace_regex_replace = {};

(obviously, fill out / uncomment the dicts as you need them)

---------------
Copyright and License
---------------

Based on work done by Katsunari Seki, Hiro Kanno for their `"Osaka" extension. <https://bitbucket.org/birkenfeld/sphinx-contrib/src/d8b73a3b5f1c/osaka/sphinxcontrib/osaka.py>`_
Also based on the multi-word replace `example from Daniweb authored by "vegaseat".<http://www.daniweb.com/software-development/python/code/216636/multiple-word-replace-in-text-python>`_ 

Except as otherwise noted (e.g., original autodoc code), this work is available under the following (MIT) License:

Copyright (c) 2012 James King

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use, copy,
modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject to the
following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.